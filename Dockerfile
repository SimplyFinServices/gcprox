FROM openjdk:11-jre-buster

EXPOSE 8085

WORKDIR /gcprox

RUN curl -O https://download.clojure.org/install/linux-install-1.10.1.561.sh
RUN chmod +x linux-install-1.10.1.561.sh
RUN ./linux-install-1.10.1.561.sh
RUN rm linux-install-1.10.1.561.sh

COPY src/ src/
COPY resources/ resources/
COPY deps.edn .
COPY prod.cljs.edn .
COPY build.sh .
RUN chmod +x build.sh

# Download deps
RUN clojure -A:server

RUN ./build.sh

CMD ["clojure", "-A:server", "-m", "gcprox.core", "--datastore-host", "datastore", "--pubsub-host", "pubsub", "--pubsub-port", "8328", "--port", "8085", "--files", "t/clj_db.log,t/clj_pubsub.log,t/node_db.log,t/node_pubsub.log"]