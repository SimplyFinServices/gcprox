# GCPROX

Gcprox is
* a simple way to run the core Google Cloud Console emulators that Cloudsure uses.
* A tool to provide visibility and impove developer experience when working with the Cloudsure Architecture and GCP. A gui for inspecting both Datastore and Pubsub is provided.

Gcprox is prepackaged as various containers combined using docker-compose. It currently includes

* Datastore emulator
* Pubsub emulator
* Gcprox Gui -> http://localhost:8085


## Usage

* Include the `dev` script and also the `docker-compose.yml` file in the repo of your application (or anywhere that you would prefer).
* `chmod +x dev` after you copy in the `dev` script.
* `./dev` to start up the dev environment, open http://localhost:8085
* `./dev clean` if it seems like you're getting old images and want new copies. Then `./dev`
* Ask Allan if you need help.

Alternatively you can clone this repo and run docker-compose commands from the command line.

Feel free to propose and do any extra bits you feel would be useful
