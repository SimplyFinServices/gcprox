echo "-> Build App"
rm -rf resources/public/cljs-out/
clojure -m figwheel.main -O advanced -bo prod
rm -rf target/

echo "-> Pretend minify"
clojure -m deploy

# echo "-> Build Server"
# rm -rf classes
# mkdir classes
# clj -A:server -e "(compile 'gcprox.core)"
# clojure -A:uberjar --main-class gcprox.core
