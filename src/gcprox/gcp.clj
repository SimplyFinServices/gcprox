(ns gcprox.gcp
  (:require [gcprox.config :as config]
            [simply.gcp.persistence.db :as gcp.db]
            [simply.gcp.auth :as gcp.auth]
            [gcprox.tracking.router :as router]
            [simply.persistence.core :as p]
            [cheshire.core :as json]
            [clojure.pprint :refer [pprint]]
            [simply.gcp.pubsub.core :as gcp.ps]
            [clojure.string :as string]
            [clojure.set :as clojure.set]
            [gcprox.tracking.pubsub :as t.ps])
  (:import [java.time.format DateTimeFormatter]
           [java.time LocalDateTime]))


;;;; DATASTORE

(defn emulator-persitence-db [host port]
  (gcp.db/db {:client (gcp.auth/emulator-client :datastore-host host :datastore-port port)
              :project-id "dogmatix"}))


(defn query [{:keys [limit offset gql db-namespace] :or {db-namespace ""} :as q}]
  (-> q
      (dissoc :type)
      (merge
       (try
         (let [result (p/query-gql (p/gql-query :gql gql :limit limit :offset offset
                                                :db-namespace (p/db-namespace db-namespace)
                                                :keyfn identity))]
           {:type :datastore/query-result
            :data result})
         (catch Exception e
           (let [err (when-let [e (get-in (ex-data e) [:data :body])]
                       (try (json/parse-string e)
                            (catch Exception _ nil)))]
             {:type :datastore/query-failure
              :error (or err (ex-cause e) (with-out-str (pprint e)))}))))))


(comment
  (query {:gql "select __key__ from __kind__"
          :db-namespace "TESTDB"})

  (query {:gql "select __key__ from __namespace__"})

  (query {:gql "select * from answer"
          :db-namespace "Sales"})

  )


;;;; PUBSUB

(defn- ps-client []
  (gcp.ps/emulator-client {:host (config/get-value :pubsub-host) :port (config/get-value :pubsub-port)}
                          (gcp.auth/emulator-client)))

(defn- list-resource [n]
  (let [k (keyword n)
        req {:method :get :url n}
        {:keys [status body] :as res} ((ps-client) req)]
    (if-not (= 200 status)
      (do
        (pprint ("List Error: " n))
        (pprint res)
        [])
      (as-> body x
        (json/parse-string x keyword)
        (k x)
        (map (fn [{:keys [name]}] (last (string/split name #"/"))) x)))))

(defn- list-topics [] (list-resource "topics"))
(defn- list-subs [] (list-resource "subscriptions"))


(defn- create-sub [topic]
  (let [sub (str "gcprox-" topic)]
    ((ps-client) {:method :put
                :url (format "subscriptions/%s" sub)
                :body {:topic (format "topics/%s" topic)}})))


(defn- pull-messages [topic]
  (let [sub (str "gcprox-" topic)
        req {:method :post
             :url (str "subscriptions/" sub ":pull")
             :body {:returnImmediately true :maxMessages 1000}}
        {:keys [body status] :as res} ((ps-client) req)]
    (if-not (= 200 status)
      (do
        (pprint ("Sub Pull Error:" sub))
        (pprint body)
        [])
      (as-> body x
        (json/parse-string x keyword)
        (:receivedMessages x)
        (map (fn [m]
               (-> (:message m)
                   t.ps/decode-publish-message
                   (assoc :topic topic
                          :type :pubsub/publish
                          :ack-id (:ackId m))
                   )) x)
        (doall x)))))

(defn- ack [topic ack-ids]
  (when-not (empty? ack-ids)
    ((ps-client) {:method :post
                :url (str "subscriptions/gcprox-" topic ":acknowledge")
                :body {:ackIds ack-ids}})))

(defn- sort-messages [msgs]
  (->> (map (fn [{:keys [publishTime] :as msg}]
              (assoc msg :sort-time
                     (LocalDateTime/parse publishTime DateTimeFormatter/ISO_OFFSET_DATE_TIME)))
            msgs)
       (sort-by :sort-time)
       (map #(dissoc % :sort-time))))

(defn- process-pubsub-messages []
  (try
    (let [topics (set (list-topics))
          existing-topics (->> (list-subs)
                               (filter #(string/starts-with? % "gcprox-"))
                               (map #(string/replace % #"gcprox-" ""))
                               set)
          topics-to-create (clojure.set/difference topics existing-topics)]
      (doseq [topic topics-to-create]
        (create-sub topic))
      (doseq [topic topics]
        (let [msgs (pull-messages topic)
              ack-ids (map :ack-id msgs)
              sorted (sort-messages msgs)]
          (router/publish sorted)
          (ack topic ack-ids)))
      (-> (map (fn [topic]
                 {:type :pubsub/create-topic
                  :topic topic}) topics-to-create)
          (router/publish)))
    (catch Exception e
      (pprint e))))


(defonce ^:private *sub-poller (atom nil))

(defn start-polling []
  (when-not @*sub-poller
    (let [interval 3000
          t (doto (Thread.
                   #(try
                      (while (not (.isInterrupted (Thread/currentThread)))
                        (Thread/sleep interval)
                        (process-pubsub-messages))
                      (catch InterruptedException _)))
              (.start))]
      (reset! *sub-poller t))))


(defn stop-polling []
  (when-let [t @*sub-poller]
    (.interrupt t))
  (reset! *sub-poller nil))
