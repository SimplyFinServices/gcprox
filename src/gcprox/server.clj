(ns gcprox.server
  (:require [aleph.http :as http]
            [manifold.stream :as s]
            [manifold.deferred :as d]
            [byte-streams :as bs]
            [hiccup.core :as hiccup]
            [gcprox.messaging :as m]
            [gcprox.tracking.file-positions :as fp]
            [gcprox.tracking.uri-log :as ul]
            [gcprox.tracking.router :as r]
            [clojure.string :as string]
            [clojure.edn :as edn]
            [gcprox.config :as config]
            [clojure.java.io :as io]
            [ring.middleware.resource :as ring.resource]
            [ring.middleware.content-type :as ring.content-type]
            [gcprox.gcp :as gcp]
            [clojure.string]
            [simply.ops.tracing-db-reporter :as tracing]))

;;;; Websocket

(def ^:private non-websocket-request
  {:status 400
   :headers {"content-type" "application/text"}
   :body "Expected a websocket request."})

(set! *print-length* Integer/MAX_VALUE)
(defn- socket-handler
  [req]
  (d/let-flow
   [conn (d/catch (http/websocket-connection req) (fn [_] nil))]
   (if-not conn
     non-websocket-request
     (do
       (s/put! conn (pr-str (r/get-history)))
       (s/put! conn (pr-str [{:type :ops/loaded}]))
       (s/connect
        (m/subscribe m/ui-topic)
        conn)
       (s/consume #(m/publish m/from-ui-topic (edn/read-string %))  conn)
       (m/handle-from-ui-topic)
       nil))))

;;;; Web

(defn- main []
  {:status 200
   :headers {"content-type" "text/html"}
   :body (slurp (io/resource "public/index.html"))})


(defn- deets []
  {:status 200
   :headers {"content-type" "text/html"}
   :body
   (str
    "<!DOCTYPE html>"
    (hiccup/html
     [:html
      [:body
       [:pre
        (with-out-str
          (clojure.pprint/pprint
           {:file-pos (fp/get-file-positions)
            :unmatched-uri (r/get-unmatched-routes)
            :uri-log (ul/get-uri-log)}))]]]))})


(defn- gql [req]
  (if (= :post (:request-method req))
    (try
      (let [q (read-string (bs/to-string (:body req)))]
        (future (r/publish [(gcp/query q)] :ignore-history? true))
        {:status 200
         :headers {"content-type" "application/edn"}
         :body (pr-str {:query-dispatched? true})})
      (catch Exception e
        {:status 400
         :headers {"content-type" "application/edn"}
         :body (pr-str {:query-dispatched? false
                        :error (str e)})}))
    {:status 400
     :headers {"content-type" "application/edn"}
     :body (pr-str {:query-dispatched? false
                    :error "Use post"})}))


(defn- request-details [req]
  (try
    (let [request-id (last (clojure.string/split (:uri req) #"/"))
          content    (tracing/generate-request-viz request-id)]
      {:status  200
       :headers {"content-type" "text/html"}
       :body    content})
    (catch Exception e
      (clojure.pprint/pprint e)
      {:status  400
       :headers {"content-type" "text/html"}
       :body    (str "<pre>" (pr-str e) "</pre>")})))


(defn- handler [req]
  (if (clojure.string/starts-with? (:uri req) "/request-detail")
    (request-details req)
    (case (:uri req)
      "/ws"    (socket-handler req)
      "/"      (main)
      "/deets" (deets)
      "/gql"   (gql req)
      {:status 404 :body "not-found"})))


(def ^:private app
  (-> handler
      (ring.resource/wrap-resource "public")
      (ring.content-type/wrap-content-type)))


;;;; Server

(defonce ^:private *server (atom nil))

(defn start-server [& {:keys [dev?] :or {dev? false}}]
  (let [port (config/get-value :port)]
    (when-not @*server
      (if dev?
        (reset! *server (http/start-server #'app {:port port}))
        (reset! *server (http/start-server app {:port port})))
      (prn (str "Listening on " port))))
  @*server)


(defn stop-server []
  (when-let [s @*server]
    (.close s)
    (reset! *server nil)))


(comment
  (start-server :dev? true)

  (m/publish m/ui-topic "test/bar {} {}")

  (stop-server)

  )
