(ns gcprox.messaging
  (:require [manifold.bus :as bus]
            [manifold.stream :as s]))


(def event-bus (bus/event-bus))
(def ui-topic "ui")
(def from-ui-topic "from-ui")
(def new-message-topic "msg")


(defn publish [t m]
  (bus/publish! event-bus t m))


(defn subscribe [t]
  (bus/subscribe event-bus t))


(defmulti handle-ui-message (fn [{:keys [type]}] type))


(defn handle-from-ui-topic []
  (->> (subscribe from-ui-topic)
       (s/consume #(handle-ui-message %))))


