(ns gcprox.core
  (:require [gcprox.deps :as deps]
            #_[gcprox.watch :as watch]
            [gcprox.server :as server]
            [aleph.netty :as aleph.netty]
            [clojure.string :as string]
            [gcprox.messaging :as m]
           #_ [gcprox.tracking.core :as t]
            [gcprox.config :as config]
            [gcprox.gcp :as gcp])
  (:gen-class))


(defn -main [& {:strs [--port --ws-port --files --datastore-port --datastore-host --pubsub-port --pubsub-host]}]
  (let [port (if --port (Integer/parseInt --port) (config/get-value :port))
        ws-port (if --ws-port (Integer/parseInt --ws-port) port)
        files (if --files
                (->> (string/split --files  #",")
                     (map string/trim))
                [])]
    (config/set-value :port port)
    (config/set-value :ws-port ws-port)
    (when --datastore-host
      (config/set-value :datastore-host (string/trim --datastore-host)))
    (when --datastore-port
      (config/set-value :datastore-port (Integer/parseInt --datastore-port)))
    (when --pubsub-host
      (config/set-value :pubsub-host (string/trim --pubsub-host)))
    (when --pubsub-port
      (config/set-value :pubsub-port (string/trim --pubsub-port)))
    (deps/reload-deps)
    #_(doseq [f files]
      (watch/watch-file t/receive-message f))
    (gcp/start-polling)
    (aleph.netty/wait-for-close (server/start-server))
    #_(watch/stop-all)
    (gcp/stop-polling)))
