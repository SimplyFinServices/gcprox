(ns gcprox.watch
  (:require [juxt.dirwatch :as dirwatch]
            [clojure.java.io :as io]
            [clojure.string :as string])
  (:import java.io.RandomAccessFile))

(defonce ^:private *watchers (atom {}))


;;;; HELPERS

(defn- file-bits [path]
  (let [splits (string/split path #"/")]
    {:file-name (last splits)
     :dir (->> (drop-last splits)
               (string/join "/"))}))


(defn- start-reader
  "Creates RandomAccessFile and seeks till the end and returns the reader"
  [path]
  (let [r (RandomAccessFile. path "r")]
    (.seek r (.length r))
    r))


(defn- prep-reader
  "Ensureds the reader keeps a valid state, returns a tuple of
   [has-reader-state-changed? reader-instance]"
  [path action reader]
  (if (and (= :modify action) reader)
    [false reader]
    [true
     (cond
       (#{:create :modify} action)
       (do
         (when reader (.close reader))
         (start-reader path))

       (and (= :delete action) reader)
       (do
         (.close reader)
         nil)

       :else reader)]))


(defn- read-file [id reader callback]
  ;debounce
  (swap! *watchers assoc-in [id :reading?] true)
  ;if for some reason the file has less text, reset to last
  (when (> (.getFilePointer reader) (.length reader))
    (.seek reader (.length reader)))
  ;read each line and push to callback
  (loop [line (.readLine reader)]
    (when line
      (when-not (empty? line)
        (callback {:path (get-in @*watchers [id :path])
                   :pos (.getFilePointer reader)
                   :data line}))
      (recur (.readLine reader))))
  ;end debounce
  (swap! *watchers assoc-in [id :reading?] false))


(defn- wrap-callback
  "returns a function that will either read a file or sync it's state with the reader"
  [id callback]
  (fn [{:keys [file action]}]
    (when-let [{:keys [reader reading? path file-name]} (get @*watchers id)]
      (when (= file-name (.getName file))
        (let [modified? (= :modify action)
              [has-reader-changed? reader] (prep-reader path action reader)]
          (cond
            has-reader-changed?
            (swap! *watchers assoc-in [id :reader] reader)

            (and (not reading?) modified?)
            (read-file id reader callback)))))))


;;;; API

(defn stop-watcher "Stops a watcher if it exists" [id]
  (when-let [{:keys [reader watcher]} (get @*watchers id)]
    (when reader (.close reader))
    (when watcher (dirwatch/close-watcher watcher))
    (swap! *watchers dissoc id)
    nil))


(defn watchers "returns a list of watchers" []
  (->> @*watchers
       (map (fn [[id watcher]]
              {:id id
               :path (:path watcher)
               :stop (fn [] (stop-watcher id))}))))


(defn stop-all "Stops all watchers" []
  (doseq [{:keys [stop]} (watchers)]
    (stop)))


(defn watch-file
  "Will start watching a file for changes.
   When the file is updated, it will send each new line to the callback fn"
  [callback path]
  (let [{:keys [dir file-name]} (file-bits path)
        dir-exists? (.exists (io/file dir))
        file-exists? (.exists (io/file path))]
    (if-not dir-exists?
      (throw (ex-info "Directory does not exists" {:dir dir}))
      (let [id (str (java.util.UUID/randomUUID))
            {:keys [dir file-name]} (file-bits path)
            reader (when file-exists? (start-reader path))
            f (wrap-callback id callback)
            watcher (dirwatch/watch-dir f (io/file dir))]
        (swap! *watchers assoc id {:id id
                                   :path path
                                   :file-name file-name
                                   :watcher watcher
                                   :reader reader
                                   :reading? false})
        {:id id
         :path path
         :stop (fn [] (stop-watcher id))}))))



(comment
  (last (first @*watchers))


  (watch-file #(prn (str "-> " %)) "t/file.log")
  (watch-file #(prn (str "-> " %)) "t/file.copy.log")
  (watch-file #(prn (str "-> " %)) "t/2.log")

  (do
    (dotimes [n 100]
      (with-open [w (io/writer "t/file.log" :append true)]
        (.write w (str "\n1:" n))))
    (dotimes [n 100]
      (with-open [w (io/writer "t/file.copy.log" :append true)]
        (.write w (str "\n2:" n))))
    (dotimes [n 100]
      (with-open [w (io/writer "t/file.log" :append true)]
        (.write w (str "\n1:" n))))
    (dotimes [n 100]
      (with-open [w (io/writer "t/file.copy.log" :append true)]
        (.write w (str "\n2:" n)))))

  (stop-all)

  )
