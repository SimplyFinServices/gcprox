(ns gcprox.tracking.core
  (:require [gcprox.messaging :as gm]
            [clojure.string :as string]
            [gcprox.tracking.file-positions]))


(def example-message
  {:pos 1
   :file "t/db.log"
   :uri "/test/bar"})


(defn- extract-uri [m]
  (let [[uri status method req res req-params]
        (string/split (:data m) #"\|\|" 6)]
    (assoc m
           :uri uri
           :status status
           :method method
           :req req
           :res res
           :req-params req-params)))


(defn receive-message [m]
  (gm/publish gm/new-message-topic (extract-uri m)))


(comment
  (extract-uri {:data "/google.pubsub.v1.Publisher/Publish||200||POST||||\u0000\u0000\u0000\u0000\u0004\n\u000226||topic=projects%2Fdogmatix%2Ftopics%2Fevents"}))
