(ns gcprox.tracking.router
  (:require [reitit.core :as r]
            [gcprox.messaging :as gm]
            [manifold.stream :as s]
            [gcprox.tracking.pubsub :as ps]
            [clojure.pprint :refer [pprint]]))


(defmulti ^:private handle :handler)


(defmethod handle :ops/test [_]
  [{:type :ops/test :v (rand-int 10)}])

(defmethod handle :pubsub/topic [msg] (ps/handle-topic-message msg))
(defmethod handle :pubsub/publish [msg] (ps/handle-publish-message msg))
(defmethod handle :pubsub/sub [msg] (ps/handle-sub-message msg))
(defmethod handle :pubsub/sub-pull [msg] (ps/handle-subscription-pull-message msg))

(defmethod handle :pubsub/node-topic [msg] (ps/handle-node-topic-message msg))
(defmethod handle :pubsub/node-sub [msg] (ps/handle-node-sub-message msg))
(defmethod handle :pubsub/node-publish [msg] (ps/handle-node-publish-message msg))
(defmethod handle :pubsub/node-ack [msg] (ps/handle-node-ack msg))


(defonce ^:private *history (atom (clojure.lang.PersistentQueue/EMPTY)))

(defn- update-history [notifications]
  (doseq [n notifications]
    (swap! *history
           (fn [q]
             (let [q' (conj q (assoc n :_historical? true))]
               (if (> (count q') 1000)
                 (pop q')
                 q'))))))


(defn get-history []
  (vec @*history))


(defmethod gm/handle-ui-message :frontend/clear-history [_]
  (reset! *history (clojure.lang.PersistentQueue/EMPTY)))


(comment
  (reset! *history (clojure.lang.PersistentQueue/EMPTY))

  (last (get-history))
  )


(defn publish [coll & {:keys [ignore-history?] :or {ignore-history? false}}]
  (gm/publish gm/ui-topic (pr-str coll))
  (when (false? ignore-history?)
    (update-history coll)))


(def ^:private *unmatched-routes (atom #{}))


(defn get-unmatched-routes []
  @*unmatched-routes)

(def ^:private ignored-routes
  #{"/google.pubsub.v1.Subscriber/GetSubscription"
    "/google.pubsub.v1.Publisher/GetTopic"
    "/google.pubsub.v1.Subscriber/ModifyAckDeadline"})

(defn- routes []
  [["/test" :ops/test]

   ["/v1/projects/{project-id}/topics/{topic}" :pubsub/topic]
   ["/v1/projects/{project-id}/topics/{topic}:publish" :pubsub/publish]
   ["/v1/projects/{project-id}/subscriptions/{subscription}" :pubsub/sub]
   ["/v1/projects/{project-id}/subscriptions/{subscription}:pull" :pubsub/sub-pull]

   ["/google.pubsub.v1.Publisher/CreateTopic" :pubsub/node-topic]
   ["/google.pubsub.v1.Subscriber/CreateSubscription" :pubsub/node-sub]
   ["/google.pubsub.v1.Publisher/Publish" :pubsub/node-publish]
   ["/google.pubsub.v1.Subscriber/Acknowledge" :pubsub/node-ack]
   ])

(defn- route-opts [] {:syntax #{:bracket}})

(def prod-router (constantly (r/router (routes) (route-opts))))
(def dev-router #(r/router (routes) (route-opts)))

(def *router (atom prod-router))

(defn use-dev-router []
  (reset! *router dev-router))

(defn use-prod-router []
  (reset! *router prod-router))


(defn- match [msg]
  (let [match (r/match-by-path (@*router) (:uri msg))]
    (when match
      (assoc msg
             :handler (get-in match [:data :name])
             :path-params (:path-params match)))))


(defn- match-and-dispatch [msg]
  (let [{:keys [path pos]} msg]
    (try
      (let [matched (match msg)]
        (if (nil? matched)
          (if (contains? ignored-routes (:uri msg))
            (publish [{:type :ops/ignored :uri (:uri msg) :ops/path path :ops/pos pos}])
            (do
              (swap! *unmatched-routes conj (:uri msg))
              (publish [{:type :ops/un-matched :msg msg :ops/path path :ops/pos pos}])))
          (let [notifications (handle matched)
                notifications (if (map? notifications) [notifications] notifications)]
            (when-not (empty? notifications)
              (publish (->> notifications
                            (map #(assoc % :ops/path path :ops/pos pos))))))))
      (catch Exception e
        (prn "Match and Dispatch Failure")
        (prn e)
        (publish [{:type :ops/err
                   :ops/path path :ops/pos pos
                   :err (or (pprint e) (str e))}]))))
  nil)


(comment
  (match {:uri  "bad"})

  (match {:uri "/google.pubsub.v1.Subscriber/GetSubscription"})

  (match {:uri "/v1/projects/dogmatix/subscriptions/sub:pull"})

  (publish [{:foo :bar}])

  (match-and-dispatch {:pos 1
                       :path "t/db.log"
                       :uri "/test"})
  )


(defonce ^:private router-subscription (gm/subscribe gm/new-message-topic))
(defonce ^:private router-consumer (s/consume #(match-and-dispatch %) router-subscription))
