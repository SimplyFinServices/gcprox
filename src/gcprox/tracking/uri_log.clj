(ns gcprox.tracking.uri-log
  (:require [gcprox.messaging :as gm]
            [manifold.stream :as s]))


(defonce ^:private *uri-log (atom #{}))
(defonce ^:private uri-log-subscription (gm/subscribe gm/new-message-topic))


(defn- log-uri [m]
  (swap! *uri-log conj (:uri m)))


(defn get-uri-log []
  (sort @*uri-log))


(defonce ^:private uri-log-consumer (s/consume log-uri uri-log-subscription))
