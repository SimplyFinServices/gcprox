(ns gcprox.tracking.file-positions
  (:require [gcprox.messaging :as gm]
            [manifold.stream :as s]))


(defonce ^:private *file-positions (atom {}))
(defonce ^:private position-subscription (gm/subscribe gm/new-message-topic))


(defn- set-position [m]
  (let [{:keys [path pos]} m]
    (swap! *file-positions assoc path pos)))


(defn get-file-positions [] @*file-positions)


(defonce ^:private position-consumer (s/consume set-position position-subscription))
