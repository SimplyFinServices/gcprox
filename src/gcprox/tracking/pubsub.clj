(ns gcprox.tracking.pubsub
  (:require [cheshire.core :as json]
            [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [cemerick.url :as url])
  (:import java.util.Base64))


(defn parse-req [s]
  (json/parse-string
   (string/replace s "\\" "")
   keyword))

(defn parse-res [s]
  (-> s
      (string/replace "\\n" "\n")
      (string/replace "\\" "")
      (json/parse-string keyword)))


(defn- decode [to-decode]
  (String. (.decode (Base64/getDecoder) to-decode)))


(defn decode-msg [e f s]
  (try
    (let [d (f s)
          t (if (map? d)
               (or (:type d) (get d "type") "")
               "")]
      {:encoding e
       :data d
       :msg-data-type t})
    (catch Exception _ nil)))


(defn msg->param [k msg]
  (-> (str "http://foo.bar?"
           (url/url-decode (:req-params msg)))
      (url/url)
      (get-in [:query k])
      (string/split #"/")
      last))


(defn decode-publish-message [msg]
  (let [data (decode (:data msg))
        decoded (or (decode-msg :edn read-string data)
                    (decode-msg :json json/parse-string data)
                    {:encoding :?
                     :data data
                     :msg-data-type ""})]
    (assoc msg
           :msg-data-type (:msg-data-type decoded)
           :data (:data decoded)
           :encoding (:encoding decoded))))


(defn handle-topic-message [msg]
  (let [{:keys [path-params status method]} msg
        {:keys [topic]} path-params]
    (case [method status]
      ["PUT" "200"] {:type :pubsub/create-topic
                   :topic topic
                   :status :created}
      ["PUT" "409"] {:type :pubsub/create-topic
                   :topic topic
                   :status :already-exists}
      {:type :pubsub/unknown-topic-event
       :msg msg})))


(defn handle-node-topic-message [msg]
  (let [{:keys [path-params status]} msg
        topic (msg->param "name" msg)]
    (case status
      "200" {:type :pubsub/create-topic
             :topic topic
             :status :created}
      "409" {:type :pubsub/create-topic
             :topic topic
             :status :already-exists}
      {:type :pubsub/unknown-topic-event
       :msg msg})))


(defn handle-sub-message [msg]
  (let [{:keys [path-params status method]} msg
        {:keys [subscription]} path-params]
    (case [method status]
      ["PUT" "200"] {:type :pubsub/create-sub
                     :subscription subscription
                     :status :created}
      ["PUT" "409"] {:type :pubsub/create-sub
                     :topic subscription
                     :status :already-exists}
      {:type :pubsub/unknown-subscription-event
       :msg msg})))


(defn handle-node-sub-message [msg]
  (let [{:keys [path-params status]} msg
        subscription (msg->param "name" msg)]
    (case status
      "200" {:type :pubsub/create-sub
             :subscription subscription
             :status :created}
      "409" {:type :pubsub/create-topicsub
             :subscription subscription
             :status :already-exists}
      {:type :pubsub/unknown-subscription-event
       :msg msg})))


(defn handle-publish-message [msg]
  (let [{:keys [path-params status method req]} msg
        {:keys [topic]} path-params]
    (try
      (case [method status]

        ["POST" "200"]
        (let [messages (:messages (parse-req req))]
          (->> messages
               (map (fn [m]
                      (-> m
                          (assoc :type :pubsub/publish
                                 :topic topic)
                          (decode-publish-message))))
               doall
               vec))

        {:type :pubsub/unknown-publish-event
         :msg msg})

      (catch Exception e
        (do
          {:type :pubsub/publish-read-exception
           :msg msg
           :e (or (pprint e) (str e))})))))


(defn handle-node-publish-message [msg]
  (let [{:keys [path-params status method req]} msg
        topic (msg->param "topic" msg)]
    (case [method status]
      ["POST" "200"]
      {:type :pubsub/publish
       :topic topic
       :encoding :?
       :data "Node.js Message can't be decoded yet (help needed)"
       :msg-data-type "unknown node.js"}

      {:type :pubsub/unknown-publish-event
       :msg msg})))


(defn handle-node-ack [msg]
  {:type :pubsub/sub-pull
   :subscription "unknown"
   :encoding :?
   :data "Node.js Message can't be decoded yet (help needed)"
   :msg-data-type "unknown node.js"})


(defn handle-subscription-pull-message [msg]
  (let [{:keys [path-params status method res]} msg
        {:keys [subscription]} path-params]
    (try
      (case [method status]

        ["POST" "200"]
        (let [messages (:receivedMessages (parse-res res))]
          (->> messages
               (map (fn [m]
                      (->
                       {:attributes (dissoc (:message m) :data)
                        :type :pubsub/sub-pull
                        :subscription subscription
                        :data (get-in m [:message :data])}
                       (decode-publish-message))))
               doall
               vec))

        {:type :pubsub/unknown-subscribe-event
         :msg msg})

      (catch Exception e
        (do
          {:type :pubsub/subscribe-read-exception
           :msg msg
           :e (or (pprint e) (str e))})))))
