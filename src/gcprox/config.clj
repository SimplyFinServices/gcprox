(ns gcprox.config
  (:require [simply.deps :as deps]))


(defonce *config (atom {:port 8085
                        :ws-port 8085
                        :datastore-port 8080
                        :datastore-host "localhost"
                        :pubsub-host "localhost"
                        :pubsub-port 8321}))


(defn set-value [k v]
  (swap! *config assoc k v))


(defn get-value [k]
  (get @*config k))
