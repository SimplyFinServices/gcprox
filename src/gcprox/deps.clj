(ns gcprox.deps
  (:require [gcprox.gcp :as gcp]
            [gcprox.config :as config]
            [simply.deps :as deps]))

(defn reload-deps []
  (deps/set-deps! {:simply.persistence.core/db
                   (gcp/emulator-persitence-db
                    (config/get-value :datastore-host)
                    (config/get-value :datastore-port))}))

(reload-deps)
