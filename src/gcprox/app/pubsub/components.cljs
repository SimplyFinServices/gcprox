(ns gcprox.app.pubsub.components
  (:require [gcprox.app.pubsub.core :as ps]
            [re-frame.core :as rf]))


(defn message-table []
  (let [messages @(rf/subscribe [::ps/messages])]
    (when-not (empty? messages)
      [:table.table.is-striped.is-narrow.is-hoverable.is-bordered.is-size-7
       [:thead>tr
        [:th "Req"]
        [:th "Topic (encoding)"]
        [:th "Sender"]
        [:th "Type"]
        [:th "Data"]
        [:th "Attributes"]
        [:th "User"]
        [:th "Date"]]
       [:tbody
        (->> messages
             (map-indexed
              (fn [i m]
                [:tr {:key i}
                 [:td {:style {:color (:request-color m)}}
                  [:a {:href (:request-detail-link m) :target "_blank" :style {:color (:request-color m)}}
                   (:request-id m)]]
                 [:td {:style {:color (:color m)}}
                  [:span.has-text-weight-bold  (:name m)]]
                 [:td (:sender m)]
                 [:td (:msg-data-type m)]
                 [:td
                  [:details
                   [:summary [:code (:pprint/summary m)]]
                   [:pre (:pprint/data m)]]]
                 [:td
                  [:details [:summary "Attributes"] [:pre (:pprint/attributes m)]]]
                 [:td (:user-id m)]
                 [:td (:publishTime m)]])))]])))

(defn topic-list []
  (let [topics (rf/subscribe [::ps/topics])]
    (fn []
      (when-not (empty? @topics)
        [:details.content.is-size-7.mb-2
         [:summary [:strong "Topics"]]
         [:ul
          (map-indexed (fn [i topic]
                         [:li {:key i} topic]) @topics)]]))))

(defn page []
  [:div
   [topic-list]
   [message-table]
   ])
