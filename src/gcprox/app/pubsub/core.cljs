(ns gcprox.app.pubsub.core
  (:require [gcprox.app.messages :as messages]
            [re-frame.core :as rf]
            [gcprox.app.common :as c]
            [gcprox.app.state :as s]
            [clojure.string :as string]
            [cljs.pprint :refer [pprint]]))


(defn init-state [s]
  (assoc s
         ::messages #queue []
         ::topics #{}))


(s/reg-init init-state)

(rf/reg-event-db
 ::clear-messages
 (fn [db]
   (init-state db)))

(rf/reg-sub
 ::messages
 (fn [db _]
   (reverse (::messages db))))


(rf/reg-sub
 ::topics
 (fn [db _]
   (sort (::topics db))))


(def ^:private topic-colors
  {"sms"              "#4caf50"
   "email"            "#2196f3"
   "events"           "#9c27b0"
   "slack"            "#f44336"
   "collections"      "#ff9800"
   "companies"        "#ffc107"
   "contracts"        "#8bc34a"
   "details-capture"  "#ffc107"
   "leads"            "#3f51b5"
   "new-contracts"    "#e91e63"
   "simply-cron-jobs" "#795548"})



(def ^:private *request-hue (atom 0))

(def ^:private *request-hues (atom {}))

(defn- roll-over-hue []
  (let [c (+ @*request-hue 48)
        n (if (> c 360) (- c 360) c)]
    (reset! *request-hue n)
    n))

(defn- request-color [request-id]
  (if (empty? request-id)
    "white"
    (let [h (if-let [hue (get @*request-hues request-id)]
              hue
              (let [new-hue (roll-over-hue)]
                (swap! *request-hues assoc request-id new-hue)
                new-hue))]
      (str "hsl(" h "deg 92% 41%)"))
    ))


(def ^:private keep-message-total 500)
(defn- receive [db t msg]
  (let [topic (:topic msg)
        sub (:subscription msg)
        name_ (or topic sub)
        color (if (string/includes? name_ "required-actions")
                "#9e9e9e"
                (get topic-colors name_ "black"))
        ppdata (with-out-str (pprint (:data msg)))
        r-id (get-in msg [:attributes :requestid])
        t-id (get-in msg [:attributes :userid])

        msg (assoc msg :msg-type t :color color
                   :name (str (if sub "<- " "-> ") name_ " (" (name (:encoding  msg)) ")")
                   :pprint/data ppdata
                   :pprint/summary (subs (first (clojure.string/split ppdata "\n")) 0 100)
                   :pprint/attributes (with-out-str (pprint (:attributes msg)))
                   :sender (get-in msg [:attributes :sender] "")
                   :request-id (if r-id
                                 (subs r-id 0 (min 4 (count r-id))) "")
                   :request-detail-link (when r-id (str "/request-detail/" r-id))
                   :user-id (or t-id "")
                   :request-color (request-color r-id))]
    (update db ::messages c/buffered-push
            msg
            :limit keep-message-total)))


(defn- add-topic [db msg]
  (update db ::topics conj (:topic msg)))


(defmethod messages/handle-message :pubsub/publish [db msg]
  (-> db
      (receive :publish msg)
      (add-topic msg)
      (s/inc-notification :pubsub)))


(defmethod messages/handle-message :pubsub/sub-pull [db msg]
  (-> db
      (receive :sub msg)
      (add-topic msg)
      (s/inc-notification :pubsub)))


(defmethod messages/handle-message :pubsub/create-topic [db msg]
  (-> db
      (add-topic msg)))


(defmethod messages/handle-message :pubsub/create-sub [db msg]
  db)
