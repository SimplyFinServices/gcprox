(ns gcprox.app.debug.components
  (:require [re-frame.core :as rf]
            [gcprox.app.state :as s]
            [gcprox.app.messages :as messages]))


(defn messages []
  (let [m @(rf/subscribe [::s/all-messages])
        allowed (set (keys (methods messages/handle-message)))]
    [:div
     [:div.has-text-right
      [:small [:a {:href "/deets" :target "_blank"} "deets"]]]
     (->> m
          (map-indexed
           (fn [i m]
             [:details {:key i}
              [:summary
               (str (:type m))
               (when (not (contains? allowed (:type m)))
                 [:span.tag.ml-1.is-danger.is-small "No Handler defined"])]
              [:pre
               (:pprint/msg m)]])))]))
