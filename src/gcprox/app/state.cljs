(ns gcprox.app.state
  (:require [re-frame.core :as rf]))


(defonce *initers (atom []))
(defn reg-init [f]
  (swap! *initers conj f))

(defn init []
  (reduce
   (fn [db f] (f db))
   {::all-messages #queue []
    ::pages {:queries {:title "Queries" :notifications 0}
           ;  :datastore {:title "Datastore" :notifications 0}
             :pubsub {:title "Pubsub" :notifications 0}
             :info {:title [:span [:i.fas.fa-info]] :notifications 0}
             :debug {:title [:span [:i.fas.fa-bug]] :notifications 0}}
    ::current-page :queries
    ::sec-since-last-msg 0
    ::log-files {}}
   @*initers))


(rf/reg-sub
 ::all-messages
 (fn [db _]
   (reverse (::all-messages db))))


(rf/reg-sub
 ::pages
 (fn [db _]
   (::pages db)))


(rf/reg-sub
 ::current-page
 (fn [db _]
   (::current-page db)))


(rf/reg-event-db
 ::page
 (fn [db [_ page]]
   (-> db
       (assoc ::current-page page)
       (assoc-in [::pages page :notifications] 0))))

(rf/reg-event-db
 ::tick
 (fn [db _]
   (update db ::sec-since-last-msg inc)))


(rf/reg-sub
 ::sec-since-last-msg
 (fn [db _]
   (::sec-since-last-msg db)))


(rf/reg-sub
 ::log-files
 (fn [db _]
   (::log-files db)))


(defn inc-notification [db page]
  (if (= (::current-page db) page)
    db
    (update-in db [::pages page :notifications] inc)))
