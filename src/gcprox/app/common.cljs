(ns gcprox.app.common)


(defn buffered-push
  [q v & {:keys [limit] :or {limit 1000}}]
  (let [q (conj q v)]
    (if (> (count q) limit)
      (pop q)
      q)))
