(ns gcprox.app.messages
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [<! >!]]
            [cljs.reader]
            [re-frame.core :as rf]
            [gcprox.app.common :as c]
            [gcprox.app.state :as s]
            [cljs.pprint :refer [pprint]]))


(defmulti handle-message (fn [state msg] (:type msg)))

(defmethod handle-message :default [db msg]
  (s/inc-notification db :debug))


(defmethod handle-message :ops/loaded [db msg]
  (reduce (fn [db k]
            (assoc-in db [::s/pages k :notifications] 0))
          db
          (keys (::s/pages db))))


(defmethod handle-message :ops/ignored [db msg]
  db)

(rf/reg-event-db
 ::clear-messages
 (fn [db]
   (assoc db ::s/all-messages #queue [])))

(def ^:private keep-message-total 500)
(rf/reg-event-db
 ::handle-message
 (fn [db [_ msg]]
   (let [{path :ops/path pos :ops/pos} msg
         msg' (assoc msg :pprint/msg (with-out-str (pprint msg)))]
     (-> db
         (handle-message msg)
         (assoc ::s/sec-since-last-msg 0)
         (update ::s/log-files (fn [f]
                                 (if (and path pos)
                                   (assoc f path pos)
                                   f)))
         ((fn [db]
            (if (true? (:app/ignore? msg'))
              db
              (update db ::s/all-messages c/buffered-push msg' :limit keep-message-total))))))))


;;;; CHANNEL

(defonce ^:private messages-chan (async/chan))
(defonce ^:private *event-loop (atom nil))

(defn receive-messages [m]
  (async/put! messages-chan m))


(defn- process-messages [m]
  (let [coll (cljs.reader/read-string m)]
    (doseq [msg coll]
      (rf/dispatch [::handle-message msg]))))


(defn start-event-loop []
  (when-not @*event-loop
    (reset!
     *event-loop
     (go (loop [m (<! messages-chan)]
           (when m
             (try
               (process-messages m)
               (catch js/Object e
                 (.error js/console e)))
             (recur (<! messages-chan))))))))
