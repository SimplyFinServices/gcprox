(ns ^:figwheel-hooks
    gcprox.app.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [goog.events :as events]
            [gcprox.app.messages :as messages]
            [gcprox.app.state :as s]
            [gcprox.app.queries.components :as queries.comp]
            [gcprox.app.queries.core :as queries]
            [gcprox.app.debug.components :as debug.comp]
            [gcprox.app.pubsub.components :as pubsub.comp]
            [gcprox.app.pubsub.core :as pubsub.core]
            #_[akiroz.re-frame.storage :refer [reg-co-fx!]])
  (:import [goog.net.WebSocket]
           [goog.Uri]))

(def *socket (atom nil))

(rf/reg-fx
 ::socket-send
 (fn [msg]
   (when @*socket
     (.send @*socket msg))))

#_(reg-co-fx! :bucks-2
            {:fx :store
             :cofx :store})

(rf/reg-event-db
 ::initialize
 (fn [_ _]
   (s/init)))


(rf/reg-sub
 ::db
 (fn [db _] db))


(rf/reg-event-fx
 ::clear-history
 (fn [_ _]
   {:dispatch-n [[::messages/clear-messages] [::pubsub.core/clear-messages]]
    ::socket-send {:type :frontend/clear-history}}))


(rf/reg-event-db
 ::request-restart
 (fn [db _]
   (assoc db ::restart? true)))

(rf/reg-sub
 ::restart?
 (fn [db _]
   (::restart? db false)))


(defn version []
  [:span.has-text-grey-light.ml-3.icon
   [:i.fas.fa-bicycle]])


(defn timer []
  [:small.has-text-grey-light.ml-3 @(rf/subscribe [::s/sec-since-last-msg])])


(defn files []
  (let [files @(rf/subscribe [::s/log-files])]
    [:div.has-text-grey-light.ml-3
     (->> files
          (map-indexed
           (fn [i [f p]]
             (let [p (/ (.round js/Math (* 100 (/ p 1000000))) 100)]
               [:small.ml-2 {:key i}
                (str f " ~" p "MB")]))))]))


(defn app []
  (let [pages @(rf/subscribe [::s/pages])
        current-page @(rf/subscribe [::s/current-page])
        restart? @(rf/subscribe [::restart?])]
    [:div
     [:div.tabs.is-small.is-boxed
      [:ul
       [:li [version]]
       (->> pages
            (map-indexed
             (fn [i [id {:keys [title notifications]}]]
               [:li {:key i
                     :class (when (= id current-page) "is-active")}
                [:a {:on-click #(rf/dispatch [::s/page id])}
                 title
                 (when-not (zero? notifications)
                   [:span.ml-1.has-background-danger.has-text-white
                    {:style {:border-radius "10px" :padding-left "5px" :padding-right "5px"
                             :font-size "0.7rem"}}
                    notifications])]])))
       [:li [:button.button.is-danger.is-light.is-small
             {:on-click #(rf/dispatch [::clear-history])}
             [:i.fas.fa-trash-alt]]]
       (when restart?
         [:li [:a.has-background-danger.has-text-light
               {:on-click #(.reload js/window.location)}
               "Please Refresh!"]])
       [:li [files]]
       [:li [timer]]
       ]]
     (case current-page
       :queries [queries.comp/query-page]
       :datastore [:div "todo"]
       :pubsub [pubsub.comp/page]
       :debug [debug.comp/messages]
       :info [:div.content
              [:ul
               [:li [:strong "Pubsub"]
                [:ul
                 [:li "Topics and Subs live only as long as the emulator. If you restart gcprox, you need to restart your apps too"]
                 [:li "Publish messages are synced every 3 seconds"]
                 [:li "Subscription pulls are not yet active"]]]
               [:li [:strong "Queries"]
                [:ul
                 [:li "New Namespaces and Kinds are synced every 10 seconds"]]]]
              [:ul
               [:li [:strong.mr-2 [:i.fas.fa-bug]] " serves as info for future development and will eventually be hidden"]
               [:li "The counter resets everytime there is communication received from the server"]]]
       [:div "Unknown Page: " current-page])]))


(defn mount []
  (r/render [app] (js/document.getElementById "app")))

(defn- tick []
  (js/setInterval
   #(rf/dispatch [::s/tick])
   1000))


(defn- tick-datastore []
  (js/setInterval
   #(queries/refresh-namespaces!)
   10000))


(defn ^:export run
  []
  (messages/start-event-loop)
  (rf/dispatch-sync [::initialize])
  (queries/refresh-namespaces!)
  (tick)
  (tick-datastore)
  (mount))


(defn ^:after-load re-render
  []
  (mount))



(defn- receive-message [m]
  (messages/receive-messages (.-data m)))


(defn- socket-closed [e]
  (rf/dispatch [::request-restart]))


(defn- open-websocket []
  (let [uri (goog.Uri. js/window.location)
        _ (.setScheme uri "ws")
        _ (.setPath uri "/ws")]
    (reset! *socket (js/WebSocket. (str uri)))
    (set! (.. @*socket -onmessage) receive-message)
    (set! (.. @*socket -onclose) socket-closed)))


(defn- listen []
  (open-websocket)
  (events/listen js/window "load" #(run))
  true)




(defonce listening (listen))
