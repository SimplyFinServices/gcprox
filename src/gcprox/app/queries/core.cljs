(ns gcprox.app.queries.core
  (:require [goog.net.XhrIo]
            [gcprox.app.messages :as messages]
            [re-frame.core :as rf]
            [clojure.string :as string]
            [cljs.pprint :refer [pprint]]))


(defn- submit-query! [query]
  (goog.net.XhrIo/send "/gql"
                       identity
                       "POST"
                       (pr-str query)))


(defn entity->ancestor [entity]
  (let [key-type (get-in entity [:simply.persistence.core/key-info :ds/key-type])
        path (get-in entity [:simply.persistence.core/key-info :ds/path])]
    (when (>= (count path) 2)
      (->> path
           (drop-last)
           (map (fn [{:keys [kind name id]}]
                  [kind (or name id)]))
           (reduce into)))))


(defn- parse-gql-string [gql]
  (try
    (let [parts (reverse (clojure.string/split gql #"\s"))]
      (loop [all-parts parts
             parts parts
             limit nil
             offset nil]

        (if (or (empty? parts) (and limit offset))

          {:gql (clojure.string/join " " (reverse all-parts))
           :limit (when limit (js/parseInt limit))
           :offset (when offset (js/parseInt offset))}

          (let [[n t] (take 2 parts)
                t' (clojure.string/upper-case t)]
            (cond
              (= "LIMIT" t')
              (recur (drop 2 all-parts) (drop 2 parts) n offset)

              (= "OFFSET" t')
              (recur (drop 2 all-parts) (drop 2 parts) limit n)

              :else
              (recur all-parts [] limit offset))))))
    (catch :default _
      {:gql gql})))


(rf/reg-sub
 ::namespaces
 (fn [db _]
   (::namespaces db [])))


(rf/reg-sub
 ::namespace-kinds
 (fn [db [_ _ns]]
   (get-in db [::namespace-kinds _ns] [])))


(defn- assoc-query-info [msg]
  (let [{:keys [db-namespace gql limit offset]} msg
        actual-gql (cond-> gql
                     limit (str " limit " limit)
                     offset (str " offset " offset))
        color-class (case (::status msg)
                      ::success "has-text-success"
                      ::failed "has-text-danger"
                      "has-text-info")
        total-rows (count (:data msg))
        more? (not (> limit total-rows))
        next-page (when (and limit more?)
                    {:db-namespace db-namespace
                     :gql (str gql " limit " limit " offset " (+ offset limit))})
        previous-page (when (and limit offset (not (zero? offset)))
                        {:db-namespace db-namespace
                         :gql (str gql " limit " limit " offset " (max 0 (- offset limit)))})]
    (assoc msg
           ::actual-gql actual-gql
           ::color-class color-class
           ::next-page next-page
           ::previous-page previous-page)))


(rf/reg-event-db
 ::start-query
 (fn [db [_ ts query]]
   (update-in db [::queries ts] merge
              (-> query
                  (assoc ::ts ts ::status ::pending)
                  assoc-query-info))))


(defn start-query!
  ([query]
   (start-query! (.getTime (js/Date.)) query))
  ([ts query]
   (let [query (merge query (parse-gql-string (:gql query)))]
     (submit-query! (assoc query ::ts ts))
     (rf/dispatch [::start-query ts query]))))


(def ^:private keep-query-total 20)
(defn- finish-query
  [db status msg]
  (let [
        ts (::ts msg)
        result (-> msg
                   (assoc ::status status)
                   assoc-query-info)
        queries (->> (assoc (::queries db) ts result)
                     vals
                     (sort-by ::ts)
                     (take-last keep-query-total)
                     (map (juxt ::ts identity))
                     (into {}))]
    (assoc db ::queries queries)))


(rf/reg-sub
 ::queries
 (fn [db _]
   (->> (::queries db)
        vals
        (sort-by ::ts)
        reverse)))


(defn refresh-namespaces! []
  (submit-query! {:app/ignore? true
                  :gql "select __key__ from __namespace__"
                  :query-type :get-namespaces}))


(defn refresh-kinds! [_ns]
  (submit-query! {:app/ignore? true
                  :gql "select __key__ from __kind__"
                  :namespace _ns
                  :db-namespace _ns
                  :query-type :get-kinds}))



(defmulti ^:private handle-query-result (fn [db msg] (:query-type msg)))


(defmethod handle-query-result :default [db msg]
  (let [{:keys [db-namespace gql limit offset]} msg
        headers
        (->> msg
             :data
             (map (fn [d]
                    (->> (:simply.persistence.core/data d)
                         (map (fn [[k v]]
                                [k (or (string? v) (boolean? v)
                                       (number? v) (keyword? v))])))))
             (reduce into)
             (group-by first)
             (map (fn [[k m]]
                    (let [indexed? (->> m
                                        (map second)
                                        (remove nil?)
                                        (reduce #(and %1 %2) true))
                          sort-query
                          (fn [dir]
                            (when indexed?
                              {:db-namespace db-namespace
                               :gql (-> gql
                                        (string/split "ORDER BY")
                                        first
                                        (str " ORDER BY `" (name k) "` " dir (if limit (str " limit " limit) "")))}))
                          sort-asc (sort-query "ASC")
                          sort-desc (sort-query "DESC")]
                      {:key k
                       :name (name k)
                       :indexed? indexed?
                       :sort-asc sort-asc
                       :sort-desc sort-desc})))
             (sort-by :key))
        data
        (->> (:data msg)
             (map (fn [d]
                    (let [ancestor (entity->ancestor d)
                          ancestor-query (when ancestor
                                           (str "select * where __key__ HAS ANCESTOR KEY("
                                                (first ancestor) ",'" (last ancestor) "')"
                                                (if limit (str " limit " limit))))
                          entity (get d :simply.persistence.core/data)
                          fields
                          (->> headers
                               (map
                                (fn [{:keys [key]}]
                                  (let [value (if (contains? entity key)
                                                (let [v (get entity key "[..null..]")]
                                                  (if (and (string? v))
                                                    (cond
                                                      (empty? v) "[..emptystr..]"
                                                      (string/blank? v) "[..blankstr..]"
                                                      :else v)
                                                    v))
                                                "[..missing..]")]
                                    (if (or (map? value) (coll? value))
                                      [:pre (with-out-str (pprint value))]
                                      (str value))))))]
                      (assoc d :ancestor ancestor :ancestor-query ancestor-query :fields fields)))))
        ]
    (finish-query db ::success (assoc msg ::headers headers ::data data))))


(defmethod handle-query-result :get-namespaces [db msg]
  (let [nss (->> (:data msg)
                 (map :simply.persistence.core/id))
        refresh? (:_historical? msg)]

    (when-not refresh?
      (refresh-kinds! "")
      (doseq [n nss]
        (refresh-kinds! n)))
    (assoc db ::namespaces (sort nss))))


(defmethod handle-query-result :get-kinds [db msg]
  (let [_ns (:namespace msg)
        kinds (->> (:data msg)
                   (map :simply.persistence.core/id))]

    (assoc-in db [::namespace-kinds _ns] (sort kinds))))


(defmethod messages/handle-message :datastore/query-result [db msg]
  (handle-query-result db msg))

(defmethod messages/handle-message :datastore/query-failure [db msg]
  (finish-query db ::failed msg))
