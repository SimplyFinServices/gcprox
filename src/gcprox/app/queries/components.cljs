(ns gcprox.app.queries.components
  (:require [re-frame.core :as rf]
            [gcprox.app.queries.core :as queries]
            [goog.events.KeyCodes]
            [clojure.string :as string]
            [cljs.pprint :refer [pprint]]))


(rf/reg-sub
 ::gql
 (fn [db _]
   (::gql db "select * from ")))


(rf/reg-event-db
 ::gql-input
 (fn [db [_ v]]
   (assoc db ::gql v)))

(def ^:private default-limit 50)
(rf/reg-sub
 ::limit
 (fn [db _]
   (::limit db default-limit)))


(rf/reg-event-db
 ::set-limit
 (fn [db [_ v]]
   (assoc db ::limit v)))


(rf/reg-sub
 ::selected-namespace
 (fn [db _]
   (::selected-namespace db "")))


(defn- default-gql [db kind]
  (if-not (empty? kind)
    (assoc db ::gql (str "select * from `" kind "` limit " (::limit db default-limit) ))
    db))


(rf/reg-event-db
 ::select-namespace
 (fn [db [_ ns]]
   (let [kind (first (get-in db [::queries/namespace-kinds ns] [""]))]
     (-> db
         (assoc ::selected-namespace ns
                ::selected-kind kind)
         (default-gql kind)))))


(rf/reg-sub
 ::selected-kind
 (fn [db _]
   (or (::selected-kind db) "")))


(rf/reg-event-db
 ::select-kind
 (fn [db [_ k]]
   (-> db
       (assoc ::selected-kind k)
       (default-gql k))))


(rf/reg-sub
 ::query
 (fn [db _]
   {:gql (string/trim (::gql db ""))
    :db-namespace (::selected-namespace db "")}))


(defn query-input []
  (let [query @(rf/subscribe [::query])
        gql @(rf/subscribe [::gql])]
    [:div.control
     [:textarea.textarea.has-fixed-size
      {:placeholder "select * from ..."
       :rows 4
       :value gql
       :on-change #(rf/dispatch [::gql-input (.. % -target -value)])
       :on-key-press (fn [e]
                       (when (= [goog.events.KeyCodes/ENTER true]
                                [(.-charCode e) (.-shiftKey e)])
                         (.preventDefault e)
                         (queries/start-query! query))
                       )}]
     [:small.has-text-grey-light.has-text-right "shift + enter for query"
      [:a.is-small.ml-3 {:href "https://cloud.google.com/datastore/docs/reference/gql_reference" :target "_blank"} "gql reference"]]]))


(defn submit-query-button []
  (let [query @(rf/subscribe [::query])]
    [:button.button.is-small
     {:on-click #(queries/start-query! query)}
     "Query"]))


(defn schema []
  (let [selected-namespace @(rf/subscribe [::selected-namespace])
        namespaces @(rf/subscribe [::queries/namespaces])
        kinds @(rf/subscribe [::queries/namespace-kinds selected-namespace])
        selected-kind @(rf/subscribe [::selected-kind])
        limit @(rf/subscribe [::limit])]
    [:div

     [:div.field.is-horizontal
      [:div.field-label.is-small
       [:label.label "Ns"]]
      [:div.field-body
       [:div.field
        [:div.control.is-expanded
         [:div.select.is-small.is-fullwidth
          [:select
           {:value selected-namespace
            :on-change #(rf/dispatch [::select-namespace (.. % -target -value)])}
           [:option {:value ""}  "[Default]"]
           (->> namespaces
                (map-indexed
                 (fn [i n]
                   [:option {:key i :value n} n])))]]]]]]

     [:div.field.is-horizontal
      [:div.field-label.is-small
       [:label.label "Kind"]]
      [:div.field-body
       [:div.field
        [:div.control.is-expanded
         [:div.select.is-small.is-fullwidth
          [:select
           {:value selected-kind
            :on-change #(rf/dispatch [::select-kind (.. % -target -value)])}
           [:option {:value ""}  "Select Kind"]
           (->> kinds
                (map-indexed
                 (fn [i n]
                   [:option {:key i :value n} n])))]]]]]]
     [:div.field.is-horizontal
      [:div.field-label.is-small
       [:label.label "Limit"]]
      [:div.field-body
       [:div.field
        [:div.control.is-expanded
         [:input.input.is-small
          {:type "number"
           :value limit
           :on-change #(rf/dispatch [::set-limit (.. % -target -value)])}]]]]]
     [:div.field.is-horizontal
      [:div.field-label.is-small]
      [:div.field-body
       [:div.field
        [:div.control
         [submit-query-button]]]]]]))


(defn queries []
  (let [queries @(rf/subscribe [::queries/queries])]
    [:div.is-size-7
     (->> queries
          (map-indexed
           (fn [i q]
             (let [{:keys [db-namespace gql limit offset]} q
                   {actual-gql ::queries/actual-gql
                    color-class ::queries/color-class
                    ts ::queries/ts
                    status ::queries/status
                    headers ::queries/headers
                    next-page ::queries/next-page
                    previous-page ::queries/previous-page} q
                   label (fn []
                           [:label
                            [:strong {:class color-class}
                             (if (empty? db-namespace) "Default" db-namespace)]
                            [:span " | " actual-gql]])
                   wrapper (if (zero? i)
                             (fn [b] [:div [:div.mb-2 [label]] b [:br]])
                             (fn [b] [:details [:summary [label]] b [:br]]))]
               [:div {:key i}
                [wrapper
                 (case status
                   ::queries/success
                   [:div.table-container.mb-0
                    [:table.table.is-striped.is-narrow.is-hoverable.is-fullwidth.is-bordered.mb-2
                     [:thead
                      [:tr
                       [:th {:style {:white-space "nowrap"}} "Id"]
                       (->> headers
                            (map-indexed
                             (fn [i {:keys [key name indexed? sort-asc sort-desc]}]
                               [(if indexed? :th :td)
                                {:key i
                                 :style {:white-space "nowrap"}}
                                name
                                (when sort-asc
                                  [:a.icon {:on-click #(queries/start-query! ts sort-asc)}
                                   [:i.fas.fa-sort-up]])
                                (when sort-desc
                                  [:a.icon {:on-click #(queries/start-query! ts sort-desc)}
                                   [:i.fas.fa-sort-down]])])))]]
                     [:tbody
                      (->> (::queries/data q)
                           (map-indexed
                            (fn [i data]
                              (let [{:keys [ancestor ancestor-query fields]} data]
                                [:tr {:key i}
                                 [:th {:style {:white-space "nowrap"}}
                                  (str (get data :simply.persistence.core/id ""))
                                  [:br]
                                  (when ancestor
                                    [:small {:style {:font-weight "normal"}} " "
                                     [:a {:on-click #(rf/dispatch [::gql-input ancestor-query])}
                                      (str ancestor)]])]
                                 (->> fields
                                      (map-indexed
                                       (fn [j value]
                                         [:td {:key j} value])))]))))]]
                    (when (or next-page previous-page)
                      [:div.buttons
                       (when previous-page
                         [:button.button.is-small {:on-click #(queries/start-query! ts previous-page)} "<"])
                       (when next-page
                         [:button.button.is-small {:on-click #(queries/start-query! ts next-page)} ">"])])]
                   ::queries/failed
                   [:pre (str (:error q))]
                   [:div])]]))))]))


(defn query-page []
  [:div
   [:div.columns
    [:div.column.is-narrow
     [schema]]
    [:div.column
     [query-input]]]
   [:hr.mt-0]

   [queries]])
