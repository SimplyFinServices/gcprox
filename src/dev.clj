(ns dev
  (:require [gcprox.deps]
            [gcprox.tracking.router :as tracking.router]
            [gcprox.server :as server]
            [gcprox.config :as config]
            [gcprox.watch :as watch]
            [gcprox.tracking.core :as tracking]
            [gcprox.gcp :as gcp]))


(defn start-dev []
  (tracking.router/use-dev-router)
  (server/start-server :dev? true)
  (clojure.java.browse/browse-url (str "http://localhost:" (config/get-value :port))))


(defn stop-dev []
  (tracking.router/use-prod-router)
  (server/stop-server))


(defn start-watch []
  (watch/watch-file #(tracking/receive-message %) "t/clj_pubsub.log")
  (watch/watch-file #(tracking/receive-message %) "t/clj_db.log")
  (watch/watch-file #(tracking/receive-message %) "t/node_pubsub.log")
  )


(comment

  (start-dev)

  (gcp/start-polling)
  (gcp/stop-polling)

  (start-watch)
  (watch/stop-all)

  (watch/watchers)


  (stop-dev)

 )
