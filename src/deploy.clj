(ns deploy
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))


(defn update-index [file-name]
  (let [path "resources/public/index.html"
        content (slurp (io/file path))
        min-content (string/replace content #"\/app.js(\?[0-9]+)?" file-name)]
    (spit (io/file path) min-content)))

(defn rename [file-name]
  (let [base "resources/public/cljs-out/"]
    (io/copy (io/file (str base "dev-main.js"))
             (io/file (str base file-name)))))

(defn deploy []
  (let [ts (.getTime (java.util.Date.))
        file-name (str "/app.js?" ts)]
    (update-index file-name)
    #_(rename file-name)))


(defn -main [& args]
  (deploy))
